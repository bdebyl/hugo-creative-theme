function toggleNav() {
  var navMenu = document.getElementById("collapse");

  if (navMenu.style.display == "none") {
    navMenu.style.display = "block";
    navMenu.style.visibility = "visible";
  } else {
    navMenu.style.display = "none";
    navMenu.style.visibility = "hidden";
  }
}
